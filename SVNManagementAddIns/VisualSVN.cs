﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SVNManagementAddIn
{
    class VisualSVN
    {
        public static string ROOT = "root\\VisualSVN";
        public static string _Service = "VisualSVN_Service";
        public static string _Repository = "VisualSVN_Repository";
        public static string _Group = "VisualSVN_Group";
        public static string _User = "VisualSVN_User";
        public static string _PermissionEntry = "VisualSVN_PermissionEntry";
    }
}
